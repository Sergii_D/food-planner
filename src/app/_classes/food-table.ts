import { Meal } from '../_classes/meal';

export class FoodTable {
	id: number;
	name: string;
	peopleNumber: number;
	mealList: number[];

	constructor() {
		this.name =  '';
		this.mealList = [];
		this.peopleNumber = 1;
	 }
}