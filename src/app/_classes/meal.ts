import { MealType } from '../_classes/meal-type';

export class Meal {
	id: number;
	name: string;
	type: number;
	productList: number[];

	constructor(name:string, type: number, productList: number[]) {
		this.name =  name;
		this.type = type;
		this.productList = productList;
	 }
}