export class Product {
  	id: number;
  	product: string;
  	soup: number;
  	porridge: number;

  	constructor(productName: string, soup:number, porrige:number) {
  		this.product = productName;
  		this.soup = soup;
  		this.porridge = porrige;
  	}
}