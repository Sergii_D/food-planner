import { Component, OnInit } from '@angular/core';
import { Observable ,  Subscription ,  forkJoin } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { FormBuilder, FormGroup,  FormControl, Validators} from '@angular/forms';

import * as _ from 'lodash';

import { ProductsService } from '../services/products.service';
import { MealsService } from '../services/meals.service';
import { FoodTablesService } from '../services/food-tables.service';
import { MealTypeService } from '../services/meal-type.service';

import { FoodTable } from '../_classes/food-table';
import { Product } from '../_classes/product'
import { Meal } from '../_classes/meal';
import { MealType } from '../_classes/meal-type';

@Component({
  selector: 'app-food-table',
  templateUrl: './food-table.component.html',
  styleUrls: ['./food-table.component.css']
})
export class FoodTableComponent implements OnInit {

    foodTable: FoodTable;
	products: Product[];
	meals: Meal[];
    mealTypes: MealType[];
    pageLoading: number = 0;
    subscription: Subscription;
    totalForProduct: number[] = [];
    dragableMealOldIndex: number;

    mealForm : FormGroup = new FormGroup({
      category: new FormControl()
    });

  	constructor(private productsService: ProductsService,
  				      private mealService: MealsService,
                private foodTableService: FoodTablesService,
                private mealTypeService: MealTypeService,
                private fb: FormBuilder) { 
        this.mealForm = this.fb.group({
            selectedMealId: ['', Validators.required],
        });
      }

  	ngOnInit() {
        // this.getProducts();
        // this.getMeals();
        // this.getMealTypes();
        // this.getFoodTable();
        this.getAllData();
        // this.temp();
  	}

    ngOnDestroy() {
        // unsubscribe to ensure no memory leaks
        this.subscription.unsubscribe();
    }

    getAllData(): void {
      this.pageLoading ++;
        let pr1 = this.productsService.getHttpProducts();
        let pr2 = this.mealService.getMeals();
        let pr3 = this.mealTypeService.getMealTypes();

        forkJoin([pr1, pr2, pr3]).subscribe(results => {
          this.products = results[0];
          this.meals = results[1];
          this.mealTypes = results[2];
          this.getFoodTable();
          this.pageLoading--;
        }, err => {
          console.log(err);
          this.pageLoading = 0;

        });
    }

    // getAllData(): void {
    //     this.pageLoading++;
    //     this.productsService.getHttpProducts()
    //         .subscribe(products => {
    //             this.products = products;
    //             this.pageLoading--;
    //         }, err=> {
    //             console.log(err);
    //         });
    // }

  	getProducts(): void {
        this.pageLoading++;
        this.productsService.getHttpProducts()
	        .subscribe(products => {
                this.products = products;
                this.pageLoading--;
            }, err=> {
                console.log(err);
            });
	}

  	getMeals(): void {
        this.pageLoading++;
  		this.mealService.getMeals()
      		.subscribe(meals => {
                this.meals = meals;
                this.pageLoading--;
            });
  	}

    getFoodTable(): void {
        this.pageLoading++;
        this.subscription = this.foodTableService.getFoodTable(0)
            .subscribe(foodTable => {
                this.foodTable = foodTable;
                this.pageLoading--;
                console.log(this.foodTable);
                this.countTotalForProduct();
            });
    }

    getMealTypes(): void {
        this.pageLoading++;
        this.mealTypeService.getMealTypes()
            .subscribe(mealTypes => {
                this.pageLoading--;
                this.mealTypes = mealTypes;
            });
    }

    addMealToFoodTable(): void {
        if(this.mealForm.valid){
            this.pageLoading++;

            let newMealId = +this.mealForm.value.selectedMealId;
            this.foodTable.mealList.push(newMealId);
            this.foodTableService.addMealToFoodTable(this.foodTable)
                .subscribe(foodTable => {
                    this.foodTable = foodTable;
                    this.pageLoading--;
                    this.countTotalForProduct();
                });
        }
    }

    deleteMealFromTable(mealId: number): void {
        this.foodTableService.deleteMealFromTable(this.foodTable, mealId)
            .subscribe(foodTable => {
                this.foodTable = foodTable;
                this.countTotalForProduct();
            });
    }

    findMeal(mealId: number): Meal {
        let searchMeal: Meal;
        searchMeal =_.find(this.meals,{id:mealId});
        return searchMeal;
    }

    findMealType(mealTypeId: number): MealType {
        let searchMealType: MealType;
        searchMealType = _.find(this.mealTypes, {id: +mealTypeId});
        return searchMealType;
    }

    countTotalForProduct(): void {
        this.totalForProduct = [];
        for (let i = 0; i < this.products.length; ++i) {
            let value: number = 0;
            for (let j = 0; j < this.foodTable.mealList.length; ++j) {
                let meal: Meal = this.findMeal(this.foodTable.mealList[j]);
                if(meal.productList.indexOf(this.products[i].id) >= 0){
                    value += (this.products[i][this.findMealType(meal.type).name])*this.foodTable.peopleNumber;
                }
            }
            this.totalForProduct.push(value);
        }

    }

    changeMealsOrder(newIndex: number): void{
        this.foodTable.mealList.splice(newIndex, 0, this.foodTable.mealList.splice(this.dragableMealOldIndex, 1)[0]);
        this.foodTableService.updateFoodTable(this.foodTable)
            .subscribe(foodTable => {
                this.foodTable = foodTable;
            });
    }

    setDragableMeal(index: number): void {
        this.dragableMealOldIndex = index;
    }

}
