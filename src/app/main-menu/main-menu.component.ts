import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { FoodTablesService } from '../services/food-tables.service';

import { FoodTable } from '../_classes/food-table';

@Component({
  selector: 'app-main-menu',
  templateUrl: './main-menu.component.html',
  styleUrls: ['./main-menu.component.css']
})
export class MainMenuComponent implements OnInit {

	activButton: string;
	navbarShown: boolean = false;
	foodTable: FoodTable

  constructor(private router: Router,
  			  private foodTableService: FoodTablesService) { }

  ngOnInit() {
  	this.foodTableService.getFoodTable(0)
  	.subscribe(foodTable => {
  		this.foodTable = foodTable;
  	});
  }


  setActivebutton(name: string): void {
  	this.activButton = name;
  }

  toggleNavbar(): void {
  	this.navbarShown = !this.navbarShown;
  }

  changePeopleNumber(): void {
  	this.foodTableService.updateFoodTable(this.foodTable)
  	.subscribe(foodTable => {
  		this.foodTable = foodTable;
  		this.foodTableService.updateFoodTableSubject(foodTable);
  	});
  }

}
