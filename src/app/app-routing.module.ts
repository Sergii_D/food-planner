import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FoodTableComponent }   from './food-table/food-table.component';
import { ProductComponent } from './product/product.component'
import { MealComponent } from './meal/meal.component'

const routes: Routes = [
  { path: '', redirectTo: '/foodtable', pathMatch: 'full' },
  { path: 'foodtable', component: FoodTableComponent },
  { path: 'product/:id', component: ProductComponent },
  { path: 'product', component: ProductComponent },
  { path: 'meal/:id', component: MealComponent },
  { path: 'meal', component: MealComponent },

];

@NgModule({
	imports: [ RouterModule.forRoot(routes) ],
  	exports: [ RouterModule ]
})
export class AppRoutingModule { }
