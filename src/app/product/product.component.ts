import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup,  FormControl, Validators} from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { ProductsService } from '../services/products.service';

import { Product } from '../_classes/product';


@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {

	productForm : FormGroup = new FormGroup({
		product: new FormControl(),
		soup: new FormControl(),
		porridge: new FormControl()
    });

    private id: number;

	constructor(private fb: FormBuilder,
	  			private route: ActivatedRoute,
	  			private router: Router,
	  			private productService: ProductsService) {	}

	ngOnInit() {
		this.checkForProduct();
	}

	checkForProduct():void {
		this.id = +this.route.snapshot.paramMap.get('id');
		if(this.id){
			this.productService.getProduct(this.id)
			.subscribe(product => {		
				this.createEditForm(product);
			});
		} else {
			this.createNewForm();
		}
	}

	getProduct(): void {

	}

	createNewForm(): void {
	  	this.productForm = this.fb.group({
	        product: ['', Validators.required],
			soup: ['', [Validators.required, Validators.min(0)]],
			porridge: ['', [Validators.required, Validators.min(0)]]
	    });
	}

	createEditForm(product: Product): void {
		this.productForm = this.fb.group({
	        product: [product.product, Validators.required],
			soup: [product.soup, [Validators.required, Validators.min(0)]],
			porridge: [product.porridge, [Validators.required, Validators.min(0)]]
	    });
	}

	submitProduct():void {
		if (this.productForm.valid) {
			if (this.id) {
				this.productService.updateProduct(this.productForm.value, this.id)
				.subscribe(product => {
					this.router.navigate(['/foodtable']);
				});
			} else {
				this.productService.addProduct(this.productForm.value)
				.subscribe(product => {
					console.log(product);
					this.router.navigate(['/foodtable']);
				});
			}
		} else {
			Object.keys(this.productForm.controls).forEach(field => { 
			  const control = this.productForm.get(field);            
			  control.markAsTouched({ onlySelf: true });       
			});
			console.log("invalidForm");
		}
	}


}
