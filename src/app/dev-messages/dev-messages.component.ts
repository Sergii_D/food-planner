import { Component, OnInit } from '@angular/core';
import { ProductsService } from '../services/products.service'

import { Product } from '../_classes/product'

@Component({
  selector: 'app-dev-messages',
  templateUrl: './dev-messages.component.html',
  styleUrls: ['./dev-messages.component.css']
})
export class DevMessagesComponent implements OnInit {

	product: Product;

  constructor(public productService: ProductsService) { }

  ngOnInit() {
  }

}
