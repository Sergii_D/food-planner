import { Component, OnInit } from '@angular/core';

import { FormBuilder, FormGroup,  FormControl, Validators} from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { ProductsService } from '../services/products.service';
import { MealsService } from '../services/meals.service';
import { MealTypeService } from '../services/meal-type.service';

import { Product } from '../_classes/product';
import { Meal } from '../_classes/meal';
import { MealType } from '../_classes/meal-type';

@Component({
  selector: 'app-meal',
  templateUrl: './meal.component.html',
  styleUrls: ['./meal.component.css']
})
export class MealComponent implements OnInit {

    products: Product[];
    mealTypes: MealType[];

    mealForm : FormGroup = new FormGroup({
      name: new FormControl(),
      type: new FormControl(),
      productList: new FormControl()
    });

    private id: number;

  	constructor(private route: ActivatedRoute,
                private router: Router,
                private productsService: ProductsService,
                private mealTypeService: MealTypeService,
                private fb: FormBuilder,
                private mealsService: MealsService) { }

  	ngOnInit() {
  	    this.checkMeal();
        this.getProducts();
        this.mealTypeService.getMealTypes()
        .subscribe(mealTypes => this.mealTypes = mealTypes);
  	}

    getProducts(): void {
      this.productsService.getHttpProducts()
       .subscribe(products => this.products = products);
    }

  	checkMeal(): void {
  		this.id = +this.route.snapshot.paramMap.get('id');
  		if (this.id) {
  			console.log(this.id);
            this.mealsService.getMeal(this.id)
            .subscribe(meal => { 
              this.createEditForm(meal);
             });
  		} else {
              this.createNewForm();
        }
  		
  	}

    createNewForm(): void {
        this.mealForm = this.fb.group({
            name: ['', Validators.required],
            type: ['', Validators.required],
            productList: [[]]
        });
    }

    createEditForm(meal: Meal): void {
        this.mealForm = this.fb.group({
            name: [meal.name, Validators.required],
            type: [meal.type, Validators.required],
            productList: [meal.productList]
        });
    }


    submitMeal(): void {
        if (this.mealForm.valid) {
            if (this.id) {
                this.mealsService.updateMeal(this.mealForm.value, this.id)
                .subscribe(meal => {
                  this.createEditForm(meal);
                  this.router.navigate(['/foodtable']);
                });
            } else {
                this.mealsService.addMeal(this.mealForm.value)
                .subscribe(meal => this.createEditForm(meal));
            }
        } else {
          Object.keys(this.mealForm.controls).forEach(field => {
            const control = this.mealForm.get(field);            
            control.markAsTouched({ onlySelf: true });       
          });
        }
    }

    toggleProduct(productId: number): void {
        let productIndex = this.mealForm.controls.productList.value.indexOf(productId);
        if(productIndex < 0){
            this.mealForm.controls.productList.value.push(+productId);
        } else {
            this.mealForm.controls.productList.value.splice(productIndex, 1);
        }
    }
}
