import { Pipe, PipeTransform } from '@angular/core';
import { Meal } from '../_classes/meal'

@Pipe({name: 'orderByMeal'})
export class OrderByMealPipe implements PipeTransform {
  transform(array: Meal[], second): Meal[] {
  	console.log(array, second);
  	array.sort((a: Meal, b: Meal) => {
  		// if(a.type !== b.type){
  		// 	return +a.type - +b.type;
  		// } else {
  			console.log(a, b);
  			return a.name>b.name ? 1 : -1;
   		// }
  	});
    return array;
  }
}