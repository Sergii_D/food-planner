import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule }   from '@angular/forms';

import { HttpClientModule }    from '@angular/common/http';

import { AppRoutingModule } from './/app-routing.module';

import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { LoadingModule } from 'ngx-loading';
import { NgDragDropModule } from 'ng-drag-drop';

import { AppComponent } from './app.component';
import { FoodTableComponent } from './food-table/food-table.component';
import { MealComponent } from './meal/meal.component';
import { ProductComponent } from './product/product.component';
import { DevMessagesComponent } from './dev-messages/dev-messages.component';
import { MainMenuComponent } from './main-menu/main-menu.component'

import { ProductsService } from './services/products.service';
import { MealsService } from './services/meals.service';
import { FoodTablesService } from './services/food-tables.service';
import { MealTypeService } from './services/meal-type.service';

import { OrderByMealPipe } from './_pipes/odrer-by-meal.pipe';


@NgModule({
  declarations: [
    AppComponent,
    FoodTableComponent,
    MealComponent,
    ProductComponent,
    DevMessagesComponent,
    OrderByMealPipe,
    MainMenuComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    AngularFontAwesomeModule,
    LoadingModule,
    NgDragDropModule.forRoot(),
  ],
  providers: [ProductsService, MealsService, FoodTablesService, MealTypeService],
  bootstrap: [AppComponent]
})
export class AppModule { }
