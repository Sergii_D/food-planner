import { Injectable } from '@angular/core';

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable ,  Subject ,  of } from 'rxjs';

import * as _ from 'lodash';

import { FoodTable } from '../_classes/food-table';

@Injectable()
export class FoodTablesService {

	private foodTableSubject = new Subject<FoodTable>();

  	constructor(private http: HttpClient) { }

  	getFoodTable (foodTableId: number): Observable<FoodTable> {
  		this.requestForFoodTable(foodTableId);
  		return this.foodTableSubject.asObservable();
	}

	requestForFoodTable (foodTableId: number): void{
		let localFoodTable: FoodTable;
	    this.http.get<FoodTable>(environment.apiUrl+'foodtables/'+foodTableId)
	    .subscribe(foodTable => {
	    	localFoodTable = foodTable;
	    	this.foodTableSubject.next(localFoodTable);

	    });
	}

	addMealToFoodTable(foodTable: FoodTable): Observable<FoodTable> {
		// foodTable.mealList.sort();
	    return this.updateFoodTable(foodTable);
	}

	updateFoodTable(foodTable: FoodTable): Observable<FoodTable> {
	    return this.http.put<FoodTable>(environment.apiUrl+'foodtables/'+foodTable.id, foodTable);
	}

	updateFoodTableSubject(foodTable: FoodTable): void {
		this.foodTableSubject.next(foodTable);
	}

	deleteMealFromTable(foodTable: FoodTable, mealId: number): Observable<FoodTable> {
		foodTable.mealList.splice(foodTable.mealList.indexOf(mealId), 1);
	    return this.http.put<FoodTable>(environment.apiUrl+'foodtables/'+foodTable.id, foodTable);
	}

}
