import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';

import { Observable } from 'rxjs';

import { Product } from '../_classes/product';


const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class ProductsService {

	changebleProduct: Product;

  	constructor(private http: HttpClient) {  	}

	getHttpProducts (): Observable<Product[]> {
	    return this.http.get<Product[]>(environment.apiUrl+'products');
	      // .pipe(
	      //   tap(heroes => this.log(`fetched heroes`)),
	      //   catchError(this.handleError('getHeroes', []))
	      // );
	}

	getProduct(productId: number): Observable<Product> {
	    return this.http.get<Product>(environment.apiUrl+'products/'+productId);
	}

	updateProduct(product: Product, productId: number): Observable<Product> {
		this.changebleProduct = product;
	    return this.http.put<Product>(environment.apiUrl+'products/'+productId, product);
	}

	addProduct(product: Product): Observable<Product> {
	    return this.http.post<Product>(environment.apiUrl+'products', product);
	}


}
