import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';


import { Observable ,  of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import { ProductsService } from '../services/products.service'

import { Meal } from '../_classes/meal';
import { Product } from '../_classes/product'


@Injectable()
export class MealsService {


  	constructor(private http: HttpClient) { }

	getMeals (): Observable<Meal[]> {
	    return this.http.get<Meal[]>(environment.apiUrl+'meals');
	}

    getMeal(id: number): Observable<Meal> {
        return this.http.get<Meal>(environment.apiUrl+'meals/'+id);
    }

	addMeal (meal: Meal): Observable<Meal> {
        console.log(meal);
	    return this.http.post<Meal>(environment.apiUrl+'meals', meal);
	}

    updateMeal (meal: Meal, mealId: number): Observable<Meal> {
        return this.http.put<Meal>(environment.apiUrl+'meals/'+mealId, meal);
    }

    changeProductInMeal(meal: Meal, productId: number): Observable<Meal[]> {
    	if (meal.productList.indexOf(productId) < 0) {
    		meal.productList.push(productId);
    	} else {
    		meal.productList.splice(meal.productList.indexOf(productId), 1);
    	}

	    return this.http.put<Meal[]>(environment.apiUrl+'meals/'+meal.id, meal);

    }
}


