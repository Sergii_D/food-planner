import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';

import { Observable } from 'rxjs';

import { MealType } from '../_classes/meal-type';

@Injectable()
export class MealTypeService {

  constructor(private http: HttpClient) { }

  	getMealTypes(): Observable<MealType[]> {
	    return this.http.get<MealType[]>(environment.apiUrl+'mealtype');
	}

}
