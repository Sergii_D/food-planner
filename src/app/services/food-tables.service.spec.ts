import { TestBed, inject } from '@angular/core/testing';

import { FoodTablesService } from './food-tables.service';

describe('FoodTablesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [FoodTablesService]
    });
  });

  it('should be created', inject([FoodTablesService], (service: FoodTablesService) => {
    expect(service).toBeTruthy();
  }));
});
